var s = (new Staple({})).main;

//var mainComponent = s.registerComponent('mainLayout',
//    new s.Component({
//        templateUrl: 'layouts/main.html',
//        rootContext: true,
//        renderDebounce: 50, //ms
//        autoMountSubComponents: true
//    })
//        .on.init(function(elem, context, globalEvents){})
//        .on.beforeRender(function(elem, context, globalEvents){})
//        .on.beforeController(function(elem, context, globalEvents){})
//        .on.destroy(function(elem, context, globalEvents){})
//        .on.serializeState(function(elem, context, globalEvents){})
//        .on.deSerializeState(function(elem, context, globalEvents){})
//        .controller(function (elem, context, globalEvents) {
//            elem.setAttribute('data-component-controller-init', 'true');
//            context.set('something', 'else');
//        })
//);
//
//mainComponent.mount('.css.selector');
//s.component('mainLayout').mount('.css.selector');

console.dir(s);
var componentTypes={
    'mainLayout': {
        config:{
            templateUrl: 'layouts/main.html',
            templateString: "hello {name}<br /><br />How are you doing?",
            context:{name:'mark'},
            rootContext: true,
            renderDebounce: 50, //ms
            autoMountSubComponents: true
        },
        controller:function (component) {
            console.dir(component );
        }
    }
};


Staple.registerComponent('mainLayout',new Staple.Component(componentTypes.mainLayout.config).controller(componentTypes.mainLayout.controller));

Staple.domParse();

Staple.on.ready.trigger();
//var l = Layout();

//s.mountLayout('site/layouts/main.html');

//
//$(document).on('ready', function () {
//
//    s.watch('_', function () {
//        console.log('EVENT:', arguments);
//    });
//
//    s.set('opa-color', 'red');
//    s.set('opa', true);
//
//
//    s.bind('customer.name customer.surname').html('[data-id="featured"]', 'Customer: {customer.name} {customer.surname}');
//    s.bind('card-class').attr('.card', 'data-class', 'card {card-class}');
//    s.bind('warn-card').class('.card', 'card-warning');
//    s.bind('opa').css('.card',
//        {opacity: '0.5', color: 'initial'},
//        {opacity: '1', color: '{opa-color}'}
//    );
//
//    s.update({
//        'customer.name': 'Mark',
//        'customer.surname': 'Holtzhausen',
//        'customer.address': {
//            'physical': {
//                streetAddress: 'Media24 Building, 40 Heerengracht',
//                suburb: 'Foreshre',
//                postalCode: '8001'
//            },
//            'postal': {
//                streetAddress: 'Media24 Building, 40 Heerengracht',
//                suburb: 'Foreshre',
//                postalCode: '8001'
//            }
//        }
//    });
//
//    s.init();
//    console.log(s.all());
//});
