(function (mount) {

    /**
     * Staple Minimal Binding Library
     * @method Staple
     * @param {} obj
     * @returns {void}
     */
    var Staple = function (obj) {
        const S_ATTR_CSS = 'data-s-original-css';
        var Staple = this;
        var watches = {};

        /**
         * Clone a passed simple object
         * @method clone
         * @param {} mixed
         * @returns CallExpression
         */
        function clone(mixed) {
            return JSON.parse(JSON.stringify(mixed));
        }

        /**
         * Test for Plain Old JavaScript Object
         * @method isPojo
         * @param {} mixed
         * @returns LogicalExpression
         */
        function isPojo(mixed) {
            return typeof(mixed) == 'object' && mixed !== null && !Array.isArray(mixed);
        }

        /**
         * Trim specified characters
         * @method trim
         * @param {} str
         * @param {} chars
         * @returns str
         */
        function trim(str, chars) {
            chars = chars || ' {}';
            chars = chars.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
            var lt = new RegExp('^[' + chars + ']*', 'g');
            var rt = new RegExp('[' + chars + ']*$', 'g');
            str = str.replace(lt, '');
            str = str.replace(rt, '');
            return str;
        }

        /**
         * Resolve the passed key
         * @method resolve
         * @param {} key
         * @param {} ensureObject
         * @returns target
         */
        function resolve(key, ensureObject) {
            target = obj;

            if (!key)return target;

            if (!isPojo(target)) {
                throw new Error('cannot resolve subkeys if the target is not an object');
            }

            var aKey = key.split('.');
            key.split('.').forEach(function (v, k, l) {
                var curKey = aKey.shift();
                if (!target.hasOwnProperty(curKey)) {
                    if (ensureObject) {
                        target[curKey] = {};
                    } else {
                        throw new Error('Key does not exist in object');
                    }
                }
                if (ensureObject && !isPojo(target[curKey])) target[curKey] = {};
                target = target[curKey];
            });
            return target;
        }

        /**
         * Split the key into leaf and root
         * @method keyLeafRoot
         * @param {} key
         * @returns ObjectExpression
         */
        function keyLeafRoot(key) {
            if (key.indexOf('.') == -1)return {root: null, leaf: key, shrub: key};
            var aKey = key.split('.');
            var leaf = aKey.pop();
            var root = aKey.join('.');
            return {root: root, leaf: leaf, shrub: root + "." + leaf};
        }

        /**
         * Calculate Affected Nodes
         * @method affectedNodes
         * @param {} node
         * @param {} prefix
         * @returns nodes
         */
        function affectedNodes(node, prefix, comprehensive) {
            comprehensive = !!comprehensive;
            prefix = prefix ? '' + prefix + '.' : (isPojo(node) ? node + '.' : '' );
            var nodes = [];
            if (!isPojo(node)) {
                var node = resolve(node);
            }

            for (var nodeName in node) if (node.hasOwnProperty(nodeName)) {
                nodes.push(prefix + nodeName);
                if (isPojo(node[nodeName])) nodes = nodes.concat(affectedNodes(node[nodeName], prefix + nodeName));
            }

            var fNodes = [];
            nodes.forEach(function (node) {
                var swapped = false;
                fNodes.forEach(function (nNode, key, list) {
                    if ((nNode.indexOf(node) != 0) && (node.indexOf(nNode) == 0)) {
                        list[key] = node;
                        swapped = true;
                    }
                });
                if (!swapped) {
                    fNodes.push(node);
                }
            });

            return comprehensive ? nodes : fNodes;

        }


        /**
         * Trigger callbacks for the alteration of a node
         * @method trigger
         * @param {} keys
         * @param {} data
         * @returns {void}
         */
        function trigger(keys, data) {
            if (!Array.isArray(keys))keys = ('' + keys).trim().replace(/ +/g, ' ').split(' ');
            keys.forEach(function (key) {
                //deconstruct key
                if (key) {
                    if (watches.hasOwnProperty('_')) {
                        watches['_'].forEach(function (callback) {
                            callback(/*data*/data, /*affectedNode*/null, /*node*/key);
                        });
                    }
                    var keyNodeMap = key.split('.');
                    var prefix = '';
                    keyNodeMap.forEach(function (keyNode) {
                        var keyName = (prefix ? prefix + '.' : prefix) + keyNode;
                        if (watches.hasOwnProperty(keyName)) {
                            watches[keyName].forEach(function (callback) {
                                callback(/*data*/data, /*affectedNode*/keyName, /*node*/key);
                            });
                        }
                        prefix = keyName;
                    });
                }
            });//.remove() ??
        }

        obj = typeof(obj) == 'object' && obj !== null && !Array.isArray(obj) ? obj : {};

        /**
         * The main function (getter with default)
         * @method main
         * @param {} key
         * @param {} dflt
         * @returns ConditionalExpression
         */
        Staple.main = function (key, dflt) {
            var ret;
            try {
                ret = resolve(key);
            } catch (e) {
                ret = dflt;
            }
            return ret !== undefined ? clone(ret) : undefined;
        };

        Staple.main.watches = watches;

        Staple.main.instance = Staple;

        /**
         * Setter function
         * @method set
         * @param {} key
         * @param {} val
         * @returns {void}
         */
        Staple.main.set = function (key, val) {
            var key = keyLeafRoot(key);
            var node = resolve(key.root, true);
            if (!isPojo(node) && key.leaf)node = {};
            var oVal = node[key.leaf];
            node[key.leaf] = val;
            trigger(key.shrub, {newVal: val, oldVal: oVal, changed: oVal !== val, deleted: false});
        };

        /**
         * Update multiple values at once
         * @method update
         * @param {} obj
         * @returns {void}
         */
        Staple.main.update = function (obj) {
            if (!isPojo(obj))throw new Error('update requires PoJo');
            for (var key in obj) {
                Staple.main.set(key, obj[key]);
            }
        };

        /**
         * Test for a key
         * @method has
         * @param {} key
         * @returns {void}
         */
        Staple.main.has = function (key) {
            try {
                resolve(key);
                return true;
            } catch (e) {
                return false;
            }
        };

        /**
         * Convenience Increment Function
         * @method inc
         * @param {} key
         * @param {} ammount
         * @returns CallExpression
         */
        Staple.main.inc = function (key, ammount) {
            Staple.main.set(key, (parseInt(Staple.main(key, 0)) || 0) + (parseInt(ammount) || 1));
            return Staple.main(key);
        };
        /**
         * Convenience Decrement Function
         * @method dec
         * @param {} key
         * @param {} ammount
         * @returns CallExpression
         */
        Staple.main.dec = function (key, ammount) {
            Staple.main.set(key, (parseInt(Staple.main(key, 0)) || 0) - (parseInt(ammount) || 1));
            return Staple.main(key);
        };

        /**
         * Remove Function
         * @method del
         * @param {} key
         * @returns {void}
         */
        Staple.main.del = function (key) {
            var key = keyLeafRoot(key);
            if (!isPojo(node) && key.leaf)return Staple.main;
            var node = resolve(key.root, true);
            trigger([key.shrub].concat(affectedNodes(key.shrub)), {
                newVal: val,
                oldVal: oVal,
                changed: oVal == val,
                deleted: false
            });
            delete(node[key.leaf]);
        };

        /**
         * Fetch All
         * @method all
         * @returns CallExpression
         */
        Staple.main.all = function () {
            return clone(obj);
        };

        /**
         * Watch for changes on a node
         * @method watch
         * @param {} keys
         * @param {} callback
         * @returns {void}
         */
        Staple.main.watch = function (keys, callback) {
            if (typeof(callback) != 'function')throw new Error('watch requires a function as callback');
            if (!Array.isArray(keys))keys = ('' + keys).trim().replace(/ +/g, ' ').split(' ');
            keys.forEach(function (key) {
                if (key) {
                    if (!watches.hasOwnProperty(key)) watches[key] = [];
                    watches[key].push(callback);
                }
            });//.remove() ???
        };

        /**
         * Parse a string replacing references with data
         * @method parse
         * @param {} str
         * @returns pStr
         */
        Staple.main.parse = function (str) {
            var pStr = clone(str);
            (str.match(/\{([^\}]*)\}/g) || []).forEach(function (v) {
                pStr = pStr.replace(v, Staple.main(trim(v), ''));
            });
            return pStr;
        };

        /**
         * Initialize data-watchers and bindings
         * @method init
         * @param {} str
         * @returns pStr
         */
        Staple.main.init = function () {
            var leaves = affectedNodes();
            for (var idx = 0, leaves = affectedNodes(), n = leaves.length; idx < n; idx++) {
                Staple.main.set(leaves[idx], Staple.main(leaves[idx]));
            }
        };

        Staple.main.serialize = function () {
            return JSON.stringify(Staple.main.all());
        }

        /**
         * Binding Library
         * @method bind
         * @param {} keyList
         * @returns CallExpression
         */
        Staple.main.bind = function (keyList) {
            if (!Array.isArray(keyList))keyList = ('' + keyList).trim().replace(/ +/g, ' ').split(' ');
            return (function (keyList, main) {
                return new function () {
                    var bind = this;

                    /**
                     * Description
                     * @method getElements
                     * @param {} elementMixed
                     * @returns elements
                     */
                    function getElements(elementMixed) {
                        var elements = (typeof(elementMixed) !== 'object') ? document.querySelectorAll(elementMixed) : elementMixed;
                        if (elements instanceof NodeList) elements = Array.prototype.slice.call(elements);
                        if (!Array.isArray(elements)) elements = [elementMixed];
                        return elements;
                    }

                    /**
                     * Description
                     * @method html
                     * @param {} elementMixed
                     * @param {} expression
                     * @returns {void}
                     */
                    bind.html = function (elementMixed, expression) {
                        main.watch(keyList, function () {
                            getElements(elementMixed).forEach(function (currentElement) {
                                currentElement.innerHTML = typeof(expression) == 'function' ? expresion() : main.parse(expression);
                            });
                        });
                    };

                    /**
                     * Description
                     * @method attr
                     * @param {} elementMixed
                     * @param {} attribute
                     * @param {} expression
                     * @returns {void}
                     */
                    bind.attr = function (elementMixed, attribute, expression) {
                        main.watch(keyList, function () {
                            getElements(elementMixed).forEach(function (currentElement) {
                                currentElement.setAttribute(attribute, typeof(expression) == 'function' ? expresion() : main.parse(expression));
                            });
                        });
                    };

                    /**
                     * Description
                     * @method class
                     * @param {} elementMixed
                     * @param {} classNames
                     * @returns {void}
                     */
                    bind.class = function (elementMixed, classNames) {
                        var classNames = Array.isArray(classNames) ? classNames : ('' + classNames).trim().replace(/ +/g, ' ').split(' ');

                        main.watch(keyList, function (data, affectedNode, key) {
                            getElements(elementMixed).forEach(function (currentElement) {
                                classNames.forEach(function (className) {
                                    currentElement.classList[main.has(key) && !!main(key) ? 'add' : 'remove'](main.parse(className));
                                });
                            });
                        });
                    };

                    /**
                     * Description
                     * @method css
                     * @param {} elementMixed
                     * @param {} truthyCss
                     * @param {} falsyCss
                     * @returns {void}
                     */
                    bind.css = function (elementMixed, truthyCss, falsyCss) {
                        truthyCss = isPojo(truthyCss) ? truthyCss : {};
                        falsyCss = isPojo(falsyCss) ? falsyCss : {};

                        main.watch(keyList, function (data, affectedNode, key) {
                            getElements(elementMixed).forEach(function (currentElement) {
                                var cssList = main.has(key) && !!main(key) ? truthyCss : falsyCss;

                                if (!currentElement.attributes.hasOwnProperty(S_ATTR_CSS)) {
                                    currentElement.setAttribute(S_ATTR_CSS, currentElement.getAttribute('style'));
                                } else {
                                    currentElement.setAttribute('style', currentElement.getAttribute(S_ATTR_CSS));
                                }

                                for (var prop in cssList) {
                                    currentElement.style.setProperty(prop, main.parse(cssList[prop]));
                                }
                            });
                        });
                    };

                    bind.form = new function () {
                        var form = this;
                        form.input = function (elementMixed, twoWay) {
                            twoWay = (undefined === twoWay) ? true : !!twoWay;
                            var key = keylist[0];
                            main.watch(key, function () {
                                getElements(elementMixed).forEach(function (currentElement) {
                                    if (currentElement.name = 'INPUT') {
                                        if (!currentElement.hasAttribute('data-dirty')) {
                                            currentElement.setAttribute('value', main(key));
                                        }
                                    }

                                    if (twoWay) {
                                        currentElement.on('change', function () {
                                            currentElement.setAttribute('data-dirty', true);
                                            main.set(key, currentElement.value);
                                            currentElement.removeAttribute('data-dirty');
                                        })
                                    }
                                });
                            });

                        }
                    };


                };
            })(keyList, Staple.main);
        };


        Object.defineProperty(Staple.main, '$', {
            writeable: false,

            /**
             * Description
             * @method get
             * @returns CallExpression
             */
            get: function () {
                return Staple.main.all();
            }
        });

    };

    /*** Staple.utilities */
    Staple.extend = function () {
        var options, name, src, copy, copyIsArray, clone;
        var target = arguments[0];
        var i = 1;
        var length = arguments.length;
        var deep = false;
        var hasOwn = Object.prototype.hasOwnProperty;
        var toStr = Object.prototype.toString;

        var isArray = function isArray(arr) {
            if (typeof Array.isArray === 'function') {
                return Array.isArray(arr);
            }

            return toStr.call(arr) === '[object Array]';
        };

        var isPlainObject = function isPlainObject(obj) {
            if (!obj || toStr.call(obj) !== '[object Object]') {
                return false;
            }

            var hasOwnConstructor = hasOwn.call(obj, 'constructor');
            var hasIsPrototypeOf = obj.constructor && obj.constructor.prototype && hasOwn.call(obj.constructor.prototype, 'isPrototypeOf');
            // Not own constructor property must be Object
            if (obj.constructor && !hasOwnConstructor && !hasIsPrototypeOf) {
                return false;
            }

            // Own properties are enumerated firstly, so to speed up,
            // if last one is own, then all properties are own.
            var key;
            for (key in obj) { /**/
            }

            return typeof key === 'undefined' || hasOwn.call(obj, key);
        };


        // Handle a deep copy situation
        if (typeof target === 'boolean') {
            deep = target;
            target = arguments[1] || {};
            // skip the boolean and the target
            i = 2;
        } else if ((typeof target !== 'object' && typeof target !== 'function') || target == null) {
            target = {};
        }

        for (; i < length; ++i) {
            options = arguments[i];
            // Only deal with non-null/undefined values
            if (options != null) {
                // Extend the base object
                for (name in options) {
                    src = target[name];
                    copy = options[name];

                    // Prevent never-ending loop
                    if (target !== copy) {
                        // Recurse if we're merging plain objects or arrays
                        if (deep && copy && (isPlainObject(copy) || (copyIsArray = isArray(copy)))) {
                            if (copyIsArray) {
                                copyIsArray = false;
                                clone = src && isArray(src) ? src : [];
                            } else {
                                clone = src && isPlainObject(src) ? src : {};
                            }

                            // Never move original objects, clone them
                            target[name] = extend(deep, clone, copy);

                            // Don't bring in undefined values
                        } else if (typeof copy !== 'undefined') {
                            target[name] = copy;
                        }
                    }
                }
            }
        }

        // Return the modified object
        return target;
    };
    Staple.domParse = function () {
        var $ = Staple.Q;
        $('[data-st-component]', arguments.length > 0 ? arguments[0] : undefined).forEach(function (dCom) {
            var componentName = dCom.getAttribute('data-st-component');
            if (Staple.hasComponent(componentName)) {
                Staple.getComponent(componentName).mount(dCom).render();
            }
        });

        $('[data-st-partial]', arguments.length > 0 ? arguments[0] : undefined).forEach(function (dCom) {
            var uri = dCom.getAttribute('data-st-partial');
            Staple.Mount.partial(uri, dCom);
        });
    };

    Staple.Mount = new function () {
        var Mount = this;
        Mount.partial = function (uri, elements) {
            var elems = (Array.isArray(elements) ? elements : ((elements instanceof HTMLElement) ? [elements] : Staple.Q('' + elements) ));

            Staple.fetch(uri)
                .then(function (res) {
                    return res.text();
                })
                .then(function (html) {
                    elems.forEach(function (elem) {
                        elem.innerHTML = html;
                    })
                })
            ;
        }
    };


    /*** Staple.Context */
    Staple.Context = function (obj, component) {
        return (function (c) {
            var context = (new Staple(obj)).main;

            context.watch('_', function () {
                component.render();
            });

            return context;
        })(obj);
    };

    /*** Staple.Dispatcher */
    Staple.Dispatcher = function (events) {
        return (function (events) {
            var d = new function () {
                var on = this;
                events = Array.isArray(events) ? events : [];
                var eventData = {};
                events.forEach(function (evt) {
                    eventData[evt] = [];
                });

                var eventCallback = function (name, fn) {
                    if (typeof(fn) == 'function') {
                        if (!eventData.hasOwnProperty(name)) eventData[name] = [];
                        eventData[name].push(fn);
                    }
                };

                var trigger = function (name, component) {
                    if (!eventData.hasOwnProperty(name)) eventData[name] = [];
                    eventData[name].forEach(function (fn) {
                        fn(component);
                    })
                };

                var evt;
                for (var idx in events) {
                    evt = events[idx];
                    (function (container, eventName) {
                        container[evt] = function (fn) {
                            eventCallback(eventName, fn);
                        };
                        container[evt].trigger = function (component) {
                            return trigger(eventName, component);
                        }
                    })(on, evt);
                }
            };
            return d;
        })(events);
    };

    /*** Staple.on Events [ready,loaded] */
    Staple.on = Staple.Dispatcher(['ready', 'loaded']);

    /*** Staple.Component */
    var components = {};
    Staple.Component = function (config) {
        console.log('config', config);
        var Component = this;
        Component.config = Staple.extend({
            templateUrl: null,
            templateString: "No templateString Provided",
            context: {},
            rootContext: true,
            renderDebounce: 50, //ms
            autoMountSubComponents: true
        }, config);
        //Component.config=config;

        Component.context = Staple.Context(config.hasOwnProperty('context') ? config.context : {}, Component);


        var controller = function () {/** **/
        };

        Component.controller = function (controllerFunction) {
            if (typeof(controllerFunction) == 'function') controller = controllerFunction;
            return Component;
        };


        Component.on = Staple.Dispatcher(['init', 'beforeRender', 'beforeController', 'destroy', 'serializeState', 'deSerializeState']);


        Component.mount = function (elem) {
            Component.on.init.trigger(Component);
            Component.mountpoint = Staple.Q(elem)[0];
            //Component.render();
            return Component;
        };

        Component.render = function () {
            Component.on.beforeRender.trigger(Component);
            if (Component.mountpoint instanceof HTMLElement) {
                var content = Component.context.parse(Component.config.templateString);
                Component.mountpoint.innerHTML = content;
                Component.on.beforeController.trigger(Component);
                controller(Component);
            }
        };

        Component.$ = function (selector) {
            return Staple.Q(selector, Component.mountpoint);
        };

        Component.serializeState = function () {
            return Staple.context.serialize();
        };

        Component.unSerializeState = function (serializedComponentState) {
            Staple.context.update(JSON.parse(serializedComponentState));
        };

        if (Component.config.hasOwnProperty('templateUrl')) {
            Staple.fetch(Component.config.templateUrl)
                .then(function (response) {
                    return response.text();
                })
                .then(function (body) {
                    Component.config.templateString = body;
                    Component.render();
                })
                .catch(function (err) {
                    console.dir(err);
                })
            ;
        }

    };
    Staple.registerComponent = function (name, component) {
        if (component instanceof Staple.Component) {
            components[name] = component;
        }
    };
    Staple.getComponent = function (name) {
        if (components.hasOwnProperty(name))return components[name];
    };
    Staple.hasComponent = function (name) {
        return !!Staple.getComponent(name);
    };

    /*** Staple.Component */
    Staple.Q = (function () {

        var classOnly = /^\.([\w\-]+)$/
            , doc = document
            , win = window
            , html = doc.documentElement
            , nodeType = 'nodeType'
        var isAncestor = 'compareDocumentPosition' in html ?
            function (element, container) {
                return (container.compareDocumentPosition(element) & 16) == 16
            } :
            function (element, container) {
                container = container == doc || container == window ? html : container
                return container !== element && container.contains(element)
            }

        function toArray(ar) {
            return [].slice.call(ar, 0)
        }

        function isNode(el) {
            var t
            return el && typeof el === 'object' && (t = el.nodeType) && (t == 1 || t == 9)
        }

        function arrayLike(o) {
            return (typeof o === 'object' && isFinite(o.length))
        }

        function flatten(ar) {
            for (var r = [], i = 0, l = ar.length; i < l; ++i) arrayLike(ar[i]) ? (r = r.concat(ar[i])) : (r[r.length] = ar[i])
            return r
        }

        function uniq(ar) {
            var a = [], i, j
            label:
                for (i = 0; i < ar.length; i++) {
                    for (j = 0; j < a.length; j++) {
                        if (a[j] == ar[i]) {
                            continue label
                        }
                    }
                    a[a.length] = ar[i]
                }
            return a
        }


        function normalizeRoot(root) {
            if (!root) return doc
            if (typeof root == 'string') return Q(root)[0]
            if (!root[nodeType] && arrayLike(root)) return root[0]
            return root
        }

        /**
         * @param {string|Array.<Element>|Element|Node} selector
         * @param {string|Array.<Element>|Element|Node=} opt_root
         * @return {Array.<Element>}
         */
        function Q(selector, opt_root) {
            var m, root = normalizeRoot(opt_root)
            if (!root || !selector) return []
            if (selector === win || isNode(selector)) {
                return !opt_root || (selector !== win && isNode(root) && isAncestor(selector, root)) ? [selector] : []
            }
            if (selector && arrayLike(selector)) return flatten(selector)


            if (doc.getElementsByClassName && selector == 'string' && (m = selector.match(classOnly))) {
                return toArray((root).getElementsByClassName(m[1]))
            }
            // using duck typing for 'a' window or 'a' document (not 'the' window || document)
            if (selector && (selector.document || (selector.nodeType && selector.nodeType == 9))) {
                return !opt_root ? [selector] : []
            }
            return toArray((root).querySelectorAll(selector))
        }

        Q.uniq = uniq

        return Q
    })();

    /*** Staple.fetch */
    (function (self) {
        'use strict';

        if (self.fetch) {
            return
        }

        function normalizeName(name) {
            if (typeof name !== 'string') {
                name = String(name)
            }
            if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
                throw new TypeError('Invalid character in header field name')
            }
            return name.toLowerCase()
        }

        function normalizeValue(value) {
            if (typeof value !== 'string') {
                value = String(value)
            }
            return value
        }

        function Headers(headers) {
            this.map = {}

            if (headers instanceof Headers) {
                headers.forEach(function (value, name) {
                    this.append(name, value)
                }, this)

            } else if (headers) {
                Object.getOwnPropertyNames(headers).forEach(function (name) {
                    this.append(name, headers[name])
                }, this)
            }
        }

        Headers.prototype.append = function (name, value) {
            name = normalizeName(name)
            value = normalizeValue(value)
            var list = this.map[name]
            if (!list) {
                list = []
                this.map[name] = list
            }
            list.push(value)
        }

        Headers.prototype['delete'] = function (name) {
            delete this.map[normalizeName(name)]
        }

        Headers.prototype.get = function (name) {
            var values = this.map[normalizeName(name)]
            return values ? values[0] : null
        }

        Headers.prototype.getAll = function (name) {
            return this.map[normalizeName(name)] || []
        }

        Headers.prototype.has = function (name) {
            return this.map.hasOwnProperty(normalizeName(name))
        }

        Headers.prototype.set = function (name, value) {
            this.map[normalizeName(name)] = [normalizeValue(value)]
        }

        Headers.prototype.forEach = function (callback, thisArg) {
            Object.getOwnPropertyNames(this.map).forEach(function (name) {
                this.map[name].forEach(function (value) {
                    callback.call(thisArg, value, name, this)
                }, this)
            }, this)
        }

        function consumed(body) {
            if (body.bodyUsed) {
                return Promise.reject(new TypeError('Already read'))
            }
            body.bodyUsed = true
        }

        function fileReaderReady(reader) {
            return new Promise(function (resolve, reject) {
                reader.onload = function () {
                    resolve(reader.result)
                }
                reader.onerror = function () {
                    reject(reader.error)
                }
            })
        }

        function readBlobAsArrayBuffer(blob) {
            var reader = new FileReader()
            reader.readAsArrayBuffer(blob)
            return fileReaderReady(reader)
        }

        function readBlobAsText(blob) {
            var reader = new FileReader()
            reader.readAsText(blob)
            return fileReaderReady(reader)
        }

        var support = {
            blob: 'FileReader' in self && 'Blob' in self && (function () {
                try {
                    new Blob();
                    return true
                } catch (e) {
                    return false
                }
            })(),
            formData: 'FormData' in self,
            arrayBuffer: 'ArrayBuffer' in self
        }

        function Body() {
            this.bodyUsed = false


            this._initBody = function (body) {
                this._bodyInit = body
                if (typeof body === 'string') {
                    this._bodyText = body
                } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
                    this._bodyBlob = body
                } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
                    this._bodyFormData = body
                } else if (!body) {
                    this._bodyText = ''
                } else if (support.arrayBuffer && ArrayBuffer.prototype.isPrototypeOf(body)) {
                    // Only support ArrayBuffers for POST method.
                    // Receiving ArrayBuffers happens via Blobs, instead.
                } else {
                    throw new Error('unsupported BodyInit type')
                }

                if (!this.headers.get('content-type')) {
                    if (typeof body === 'string') {
                        this.headers.set('content-type', 'text/plain;charset=UTF-8')
                    } else if (this._bodyBlob && this._bodyBlob.type) {
                        this.headers.set('content-type', this._bodyBlob.type)
                    }
                }
            }

            if (support.blob) {
                this.blob = function () {
                    var rejected = consumed(this)
                    if (rejected) {
                        return rejected
                    }

                    if (this._bodyBlob) {
                        return Promise.resolve(this._bodyBlob)
                    } else if (this._bodyFormData) {
                        throw new Error('could not read FormData body as blob')
                    } else {
                        return Promise.resolve(new Blob([this._bodyText]))
                    }
                }

                this.arrayBuffer = function () {
                    return this.blob().then(readBlobAsArrayBuffer)
                }

                this.text = function () {
                    var rejected = consumed(this)
                    if (rejected) {
                        return rejected
                    }

                    if (this._bodyBlob) {
                        return readBlobAsText(this._bodyBlob)
                    } else if (this._bodyFormData) {
                        throw new Error('could not read FormData body as text')
                    } else {
                        return Promise.resolve(this._bodyText)
                    }
                }
            } else {
                this.text = function () {
                    var rejected = consumed(this)
                    return rejected ? rejected : Promise.resolve(this._bodyText)
                }
            }

            if (support.formData) {
                this.formData = function () {
                    return this.text().then(decode)
                }
            }

            this.json = function () {
                return this.text().then(JSON.parse)
            }

            return this
        }

        // HTTP methods whose capitalization should be normalized
        var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT']

        function normalizeMethod(method) {
            var upcased = method.toUpperCase()
            return (methods.indexOf(upcased) > -1) ? upcased : method
        }

        function Request(input, options) {
            options = options || {}
            var body = options.body
            if (Request.prototype.isPrototypeOf(input)) {
                if (input.bodyUsed) {
                    throw new TypeError('Already read')
                }
                this.url = input.url
                this.credentials = input.credentials
                if (!options.headers) {
                    this.headers = new Headers(input.headers)
                }
                this.method = input.method
                this.mode = input.mode
                if (!body) {
                    body = input._bodyInit
                    input.bodyUsed = true
                }
            } else {
                this.url = input
            }

            this.credentials = options.credentials || this.credentials || 'omit'
            if (options.headers || !this.headers) {
                this.headers = new Headers(options.headers)
            }
            this.method = normalizeMethod(options.method || this.method || 'GET')
            this.mode = options.mode || this.mode || null
            this.referrer = null

            if ((this.method === 'GET' || this.method === 'HEAD') && body) {
                throw new TypeError('Body not allowed for GET or HEAD requests')
            }
            this._initBody(body)
        }

        Request.prototype.clone = function () {
            return new Request(this)
        }

        function decode(body) {
            var form = new FormData()
            body.trim().split('&').forEach(function (bytes) {
                if (bytes) {
                    var split = bytes.split('=')
                    var name = split.shift().replace(/\+/g, ' ')
                    var value = split.join('=').replace(/\+/g, ' ')
                    form.append(decodeURIComponent(name), decodeURIComponent(value))
                }
            })
            return form
        }

        function headers(xhr) {
            var head = new Headers()
            var pairs = xhr.getAllResponseHeaders().trim().split('\n')
            pairs.forEach(function (header) {
                var split = header.trim().split(':')
                var key = split.shift().trim()
                var value = split.join(':').trim()
                head.append(key, value)
            })
            return head
        }

        Body.call(Request.prototype)

        function Response(bodyInit, options) {
            if (!options) {
                options = {}
            }

            this.type = 'default'
            this.status = options.status
            this.ok = this.status >= 200 && this.status < 300
            this.statusText = options.statusText
            this.headers = options.headers instanceof Headers ? options.headers : new Headers(options.headers)
            this.url = options.url || ''
            this._initBody(bodyInit)
        }

        Body.call(Response.prototype)

        Response.prototype.clone = function () {
            return new Response(this._bodyInit, {
                status: this.status,
                statusText: this.statusText,
                headers: new Headers(this.headers),
                url: this.url
            })
        }

        Response.error = function () {
            var response = new Response(null, {status: 0, statusText: ''})
            response.type = 'error'
            return response
        }

        var redirectStatuses = [301, 302, 303, 307, 308]

        Response.redirect = function (url, status) {
            if (redirectStatuses.indexOf(status) === -1) {
                throw new RangeError('Invalid status code')
            }

            return new Response(null, {status: status, headers: {location: url}})
        }

        self.Headers = Headers;
        self.Request = Request;
        self.Response = Response;

        self.fetch = function (input, init) {
            return new Promise(function (resolve, reject) {
                var request
                if (Request.prototype.isPrototypeOf(input) && !init) {
                    request = input
                } else {
                    request = new Request(input, init)
                }

                var xhr = new XMLHttpRequest()

                function responseURL() {
                    if ('responseURL' in xhr) {
                        return xhr.responseURL
                    }

                    // Avoid security warnings on getResponseHeader when not allowed by CORS
                    if (/^X-Request-URL:/m.test(xhr.getAllResponseHeaders())) {
                        return xhr.getResponseHeader('X-Request-URL')
                    }

                    return;
                }

                xhr.onload = function () {
                    var options = {
                        status: xhr.status,
                        statusText: xhr.statusText,
                        headers: headers(xhr),
                        url: responseURL()
                    }
                    var body = 'response' in xhr ? xhr.response : xhr.responseText;
                    resolve(new Response(body, options))
                }

                xhr.onerror = function () {
                    reject(new TypeError('Network request failed'))
                }

                xhr.open(request.method, request.url, true)

                if (request.credentials === 'include') {
                    xhr.withCredentials = true
                }

                if ('responseType' in xhr && support.blob) {
                    xhr.responseType = 'blob'
                }

                request.headers.forEach(function (value, name) {
                    xhr.setRequestHeader(name, value)
                })

                xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit)
            })
        }
        self.fetch.polyfill = true
    })(Staple);


    /**
     * Description
     * @method Staple
     * @param {} obj
     * @returns MemberExpression
     */
    mount.Staple = Staple;


    Staple.on.loaded.trigger();

})(window);