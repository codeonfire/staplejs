(function (mount) {

    /**
     * Layout Minimal Binding Library
     * @method Layout
     * @param {} obj
     * @returns {void}
     */
    var Layout = function (layoutContent, mountPoint ) {
        
        var l=this;
    };

    /**
     * Description
     * @method Layout
     * @param {} obj
     * @returns MemberExpression
     */
    mount.Layout = function (layoutContent, mountPoint ) {
        return new Layout(layoutContent, mountPoint);
    };

})(window);